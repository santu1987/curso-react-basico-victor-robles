import React, {Component} from 'react';

class MiComponente extends Component{

	//Render
	render(){
		//---Armo variable receta
		let receta = {
			nombre: 'pizza',
			ingredientes:['Tomate','Queso','Jamón'],
			calorias:500
		};
		//--
		return (
			<div className="mi-componente">
				<h1>{'Receta: '+ receta.nombre}</h1>
				<h1>{'Calorías: '+ receta.calorias}</h1>
				<ol>{
						receta.ingredientes.map((ingrediente,i)=>{
							return(
									<li key={i}>
										{ingrediente}
									</li>
							)
						})
				}
				</ol>
			</div>
		);
	}

	//
}

//Exportarlo para ser usado en cualquier vista
export default MiComponente;